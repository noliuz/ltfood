const express = require('express')
var cors = require('cors')
const app = express()
app.use(cors())
const bodyParser = require("body-parser");
app.use(bodyParser.json()); //ทำให้รับ json จาก body ได้

const {seqConn} = require('./seqConn')
const Config = require('./config.js')
const {FoodModel,CategoryModel,OrderTableModel,FoodOrderModel,KitchenModel} = require('./models.js');

app.get('/get_table', async (req, res) => {
  let mres = await OrderTableModel.findAll({

    })
  res.send(mres)
})

app.get('/food_list', async (req, res) => {
  FoodModel.belongsTo(CategoryModel,{foreignKey:'category_id'})

  let mres = await FoodModel.findAll({
      include:[CategoryModel],
    })
  let resArr = []
  mres.forEach((item) => {
    let data = {
      id : item.id,
      name : item.name,
      price : item.price,
      category_name : item.category.name
    }
    resArr.push(data)
  });

  res.send(resArr)
})

app.get('/category_list', async (req, res) => {
  let mres = await CategoryModel.findAll({})
  res.send(mres)
})

app.get('/get_order_list/:tableId', async (req, res) => {
  FoodOrderModel.belongsTo(FoodModel,{foreignKey:'food_id'})

  let tableId = req.params.tableId
  let mres = await FoodOrderModel.findAll({
    where: {
      table_id:tableId,
      is_paid:false
    },
    include:[FoodModel]
  })

  res.send(mres)
})

app.post('/send_order', async (req, res) => {
  //console.log(req.body.dataArr)
  let dataArr = req.body.dataArr
  dataArr.forEach(async (item)=> {
    let dataSql = {
      food_id:item.foodId,
      amount: item.amount,
      detail: item.detail,
      table_id: item.tableId,

    }

    FoodOrderModel
      .create(dataSql)
      .then(res => { // add kitchen id
        //console.log(res.id)
        KitchenModel.create({
          order_id:res.id,
          is_finish:false
        })
      })
  })

  res.send('ok')
})

app.post('/delete_order', async (req, res) => {
  let orderId = req.body.order_id
  FoodOrderModel.destroy({
    where:{
      id:orderId
    }
  })
  res.send('ok')
})

app.get('/free_table_id', async (req, res) => {
  FoodOrderModel.findAll({
    group: ['table_id'],
    raw:true,
    where:{is_paid:0}
  })
  .then((data)=> {
    //get occupation table id
    let usedIdArr = []
    data.forEach((item)=> {
      usedIdArr.push(item.table_id)
    })
    //get table id
    OrderTableModel.findAll({
      raw:true
    }).then((otData)=> {
      let allIdArr = []
      otData.forEach((item) => {
        allIdArr.push(item.id)
      })
      let freeIdArr = allIdArr.filter(n=>!usedIdArr.includes(n))

      res.send(freeIdArr)
    })
  })
})

app.post('/move_table', async (req, res) => {
  let fromId = req.body.fromId
  let toId = req.body.toId
  FoodOrderModel.update(
    {table_id:toId},
    {where:{table_id:fromId}}
  )
  res.send('ok')
})

app.post('/cash_table', async (req, res) => {
  let tableId = req.body.table_id
  console.log(tableId)
  /*FoodOrderModel.update(
    {is_paid:1},
    {where:{table_id:tableId}}
  )*/
  seqConn.query("UPDATE food_order SET is_paid=1 WHERE table_id="+tableId)

  res.send('ok')
})

app.get('/date_total_sell/:date', async (req, res) => {
  let date = req.params.date
  let sql =  `
    SELECT food.name,food.price,food_order.amount FROM
    food_order LEFT JOIN food ON food_order.food_id=food.id
    WHERE food_order.is_paid=1
          AND DATE(food_order.updatedAt) = "` + date +'"'
  let res1 = await seqConn.query(sql)
  //console.log(res1[0])
  res.send(res1[0])
})

app.get('/get_kitchen_cooking', async (req, res) => {
  let sql = `
    SELECT kitchen.id,food.name,food_order.amount FROM kitchen
    LEFT JOIN food_order ON kitchen.order_id=food_order.id
    LEFT JOIN food ON food_order.food_id=food.id WHERE kitchen.is_finish=0
  `
  let mres = await seqConn.query(sql)
  mres = mres[0]
  res.send(mres)

})

app.post('/finish_order', async (req, res) => {
  let cookId = req.body.cook_id
  KitchenModel.update(
    {is_finish:1},
    {where:{id:cookId}}
  )
  //console.log(orderId)
  res.send('ok')
})
app.listen(Config.portNumber, () => {
  console.log(`Example app listening at http://localhost:`+Config.portNumber)
})
