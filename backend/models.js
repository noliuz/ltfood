const Sequelize = require('sequelize');
const {seqConn} = require('./seqConn')

const FoodModel = seqConn.define('food', {
  category_id : {
    type: Sequelize.INTEGER
  },
  name : {
    type: Sequelize.STRING
  },
  price : {
    type: Sequelize.FLOAT
  }
}, {
  tableName: 'food',
  timestamps: false,
})

const CategoryModel = seqConn.define('category', {
  name : {
    type: Sequelize.STRING
  },
}, {
  tableName: 'category',
  timestamps: false,
})

const FoodOrderModel = seqConn.define('food_order', {
  food_id : {
    type: Sequelize.INTEGER
  },
  amount : {
    type: Sequelize.INTEGER
  },
  detail : {
    type: Sequelize.STRING
  },
  table_id : {
    type: Sequelize.INTEGER
  }
}, {
  tableName: 'food_order',
})

const KitchenModel = seqConn.define('kitchen', {
  order_id: {
    type: Sequelize.INTEGER
  },
  is_finish : {
    type: Sequelize.BOOLEAN
  },
}, {
  tableName: 'kitchen',
  timestamps: false,
})

const OrderTableModel = seqConn.define('order_table', {
  name : {
    type: Sequelize.STRING
  },
}, {
  tableName: 'order_table',
  timestamps: false,
})

module.exports = {FoodModel,CategoryModel,FoodOrderModel,KitchenModel,OrderTableModel}
