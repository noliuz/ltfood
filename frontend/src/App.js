import './App.css';
import React from 'react'
import {Button,Grid,Select,MenuItem,Table,TableBody,TableCell,TableContainer,TableHead,TableRow,TextField,Icon} from '@material-ui/core'
import logoImg from "./logo.png"
import DeleteIcon from '@material-ui/icons/Delete'
import AddIcon from '@material-ui/icons/Add'
import HeaderComponent from './component/HeaderComponent'
import OrderComponent from './component/OrderComponent'
import SummaryComponent from './component/SummaryComponent'
import CashierComponent from './component/CashierComponent'
import AdminComponent from './component/AdminComponent'
import KitchenComponent from './component/KitchenComponent'
import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom"

function App() {


  return (

    <div className="App">
      <HeaderComponent />
      <Router basename='/ltfood'>
        <Switch>
          <Route path="/order"><OrderComponent /></Route>
          <Route path="/cash"><CashierComponent /></Route>
          <Route path="/admin"><AdminComponent /></Route>
          <Route path="/kitchen"><KitchenComponent /></Route>

        </Switch>
      </Router>
    </div>
  );
}

export default App;
