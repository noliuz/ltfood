import React from 'react'
import {Grid,Menu,MenuItem,Button,Link}from '@material-ui/core'
import logoImg from "../logo.png"
import Config from '../config'

var menuList = [
  {name:'สั่งอาหาร',link:Config.basePath+'/order'},
  {name:'เก็บเงิน/ย้ายโต๊ะ',link:Config.basePath+'/cash'},
  {name:'ดูยอดขาย',link:Config.basePath+'/admin'},
  {name:'ห้องครัว',link:Config.basePath+'/kitchen'},
]

function HeaderComponent() {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const preventDefault = (event) => event.preventDefault();
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Grid container justify='space-between'>
      <Grid item>
        <img src={logoImg} width="100" style={{marginLeft:30}}/>
      </Grid>
      <Grid item>
        <Button onClick={handleClick} style={{marginRight:50,color:'#aaaa00'}}>
          <h2>เมนู</h2>
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          {
            menuList.map((item)=> {
              return (
                <MenuItem onClick={handleClose} component='a' href={item.link}
                >
                  {item.name}
                </MenuItem>
              )
            })

          }

        </Menu>
      </Grid>
    </Grid>
  )
}

export default HeaderComponent;
