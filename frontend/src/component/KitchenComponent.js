import React,{useEffect,useState} from 'react'
import {Button,Grid,Select,MenuItem,Table,TableBody,TableCell,TableContainer,TableHead,TableRow,TextField,Icon,Dialog,DialogTitle,DialogActions} from '@material-ui/core'
import Config from '../config'

function KitchenComponent() {
  const [cookList,setCookList] = useState([])

  const onUpdateClick = async () => {
    let url = Config.API_URL+'/get_kitchen_cooking'
    fetch(url)
    .then(res=>res.json())
    .then((data) => {
      //console.log(data)
      setCookList([...data])
    })
  }
  const onFinishClick = index => e => {
    //console.log(index)
    let url = Config.API_URL+'/finish_order'
    let sendData = {cook_id:cookList[index].id}
    fetch(url,{
      method:'POST',
      body:JSON.stringify(sendData),
      headers: {
        'Content-Type': 'application/json'
      }
    })

    //remove item
    let arr = cookList
    arr.splice(index,1)
    setCookList([...arr])


  }
  return (
    <div>
      <h4>รายการในครัว</h4>
      <Button variant="contained" onClick={onUpdateClick}>อัพเดท</Button>
      <TableContainer>
        <Table>
          <TableBody>
          {
              cookList.map((item,i)=> {
                return (

                  <TableRow>
                    <TableCell>{item.name}</TableCell>
                    <TableCell>{item.amount}</TableCell>
                    <TableCell>
                      <Button variant="contained" style={{backgroundColor:'#00ee00'}} onClick={onFinishClick(i)}>
                        เสร็จแล้ว
                      </Button>
                    </TableCell>
                  </TableRow>
                )
              })

          }


          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
}

export default KitchenComponent
