import React,{useEffect,useState} from 'react'
import {Button,Grid,Select,MenuItem,Table,TableBody,TableCell,TableContainer,TableHead,TableRow,TextField,Icon,Dialog,DialogTitle,DialogActions} from '@material-ui/core'
import Config from '../config'
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import thaiLocale from "moment/locale/th";


function AdminComponent() {
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [totalResult,setTotalResult] = useState([])
  const [totalPrice,setTotalPrice] = useState(0)

  const handleDateChange = async (date) => {
    setSelectedDate(date)
    let dateStr = date.format('yyyy-MM-DD')
    let url = Config.API_URL+'/date_total_sell/'+dateStr
    //console.log(url)
    fetch(url)
    .then(res=>res.json())
    .then((data) => {
      setTotalResult(data)
      let total = 0
      data.forEach((item) => {
        total += item.amount*item.price
      })
      setTotalPrice(total)

    })
  }

  return (
    <div>
    <MuiPickersUtilsProvider utils={MomentUtils} locale='thaiLocale'>
    <KeyboardDatePicker
        disableToolbar
        variant="inline"
        format="DD/MM/yyyy"
        margin="normal"
        id="date-picker-inline"
        label="ดูยอดขาย เลือกวันที่"
        value={selectedDate}
        onChange={handleDateChange}
        KeyboardButtonProps={{
          'aria-label': 'change date',
        }}
      />
    </MuiPickersUtilsProvider>
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell><b>ชื่อ</b></TableCell>
            <TableCell><b>จำนวน</b></TableCell>
            <TableCell><b>ราคา</b></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            totalResult.map((item,i) => {
              return (
                <TableRow key={i}>
                  <TableCell>{item.name}</TableCell>
                  <TableCell>{item.amount}</TableCell>
                  <TableCell>{item.price}</TableCell>
                </TableRow>
              )
            })

          }
          <TableRow>
            <TableCell><b>รวมเงิน</b></TableCell>
            <TableCell>
              <b>
                {totalPrice}
              </b>
            </TableCell>

          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
    </div>
  )
}

export default AdminComponent
