import {Button,Grid,Select,MenuItem,Table,TableBody,TableCell,TableContainer,TableHead,TableRow,TextField,Icon,Box} from '@material-ui/core'
import HeaderComponent from './HeaderComponent'
import DeleteIcon from '@material-ui/icons/Delete'

function SummaryComponent() {
  return (
    <div>
      <HeaderComponent />
      <b>รายการที่สั่ง</b>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell><b>ชื่อ</b></TableCell>
              <TableCell><b>จำนวน</b></TableCell>
              <TableCell><b>ราคา</b></TableCell>
              <TableCell><b>เพิ่มเติม</b></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>ข้าวต้ม</TableCell>
              <TableCell>2</TableCell>
              <TableCell>50</TableCell>
              <TableCell></TableCell>
              <TableCell><DeleteIcon style={{color:'red'}}/></TableCell>
            </TableRow>
            <TableRow>
              <TableCell>ข้าวผัด</TableCell>
              <TableCell>2</TableCell>
              <TableCell>50</TableCell>
              <TableCell></TableCell>
              <TableCell><DeleteIcon style={{color:'red'}}/></TableCell>
            </TableRow>
            <TableRow>
              <TableCell><b>รวม</b></TableCell>
              <TableCell></TableCell>
              <TableCell><b>250</b></TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
      <Box m={2}>
        <Button variant="contained" color="primary" style={{marginRight:30}}>ย้ายโต๊ะ</Button>
        <Button variant="contained" color="primary">เก็บเงิน</Button>
      </Box>
    </div>
  )
}

export default SummaryComponent;
