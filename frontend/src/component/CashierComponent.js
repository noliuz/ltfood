import React,{useEffect,useState} from 'react'
import {Button,Grid,Select,MenuItem,Table,TableBody,TableCell,TableContainer,TableHead,TableRow,TextField,Icon,Dialog,DialogTitle,DialogActions} from '@material-ui/core'
import ConfirmDialog from './ConfirmDialog'
import HeaderComponent from './HeaderComponent'
import DeleteIcon from '@material-ui/icons/Delete'
import Config from '../config'

var tableArr = []
var tableNameIdArr = []
var foodNameIdPriceArr = []
var foodOrderListArr = []

function CashierComponent() {
  const [confirmCashOpen,setConfirmCashOpen] = useState(false)
  const [tableSelect,setTableSelect] = useState([])
  const [tableChoose,setTableChoose] = useState('')
  const [moveTableChoose,setMoveTableChoose] = useState('')
  const [foodOrderTable,setFoodOrderTable] = useState([])
  const [confirmDeleteOpen,setConfirmDeleteOpen] = useState(false)
  const [deleteClickIndex,setDeleteClickIndex] = useState(0)
  const [totalPrice,setTotalPrice] = useState(0)
  const [notFreeTableOpen,setNotFreeTableOpen] = useState(false)
  const [disableMoveTableButton,setDisableMoveTableButton] = useState(false)
  const [confirmMoveTableOpen,setConfirmMoveTableOpen] = useState(false)

  const onCashClick = () => {
    setConfirmCashOpen(true)
  }
  const cashTable = async () => {
    if (totalPrice == 0)
      return

    let cashTableId = tableNameIdArr.filter((item)=>item.name===tableChoose)
    cashTableId = cashTableId[0].id
    let sendData = {table_id:cashTableId}
    let url = Config.API_URL+'/cash_table'

    let res = await fetch(url,{
      method:'POST',
      body:JSON.stringify(sendData),
      headers: {
        'Content-Type': 'application/json'
      }
    })

    getFoodOrderList(cashTableId)

  }
  const onMoveTableButtonClick = () => {
    setConfirmMoveTableOpen(true)
  }
  const moveTable = async () => {
    let fromId = tableNameIdArr.filter((item)=>item.name===tableChoose)
    fromId = fromId[0].id
    let toId = tableNameIdArr.filter((item)=>item.name===moveTableChoose)
    toId = toId[0].id
    let sendData = {fromId:fromId,toId:toId}
    let url = Config.API_URL+'/move_table'

    let res = await fetch(url,{
      method:'POST',
      body:JSON.stringify(sendData),
      headers: {
        'Content-Type': 'application/json'
      }
    })

    //console.log(moveTableChoose)
    setTableChoose(moveTableChoose)
    setDisableMoveTableButton(true)
    getFoodOrderList(toId)


  }
  const onNotFreeTableClose = () => {
    setNotFreeTableOpen(false)
  }
  const onMoveTableChange = (e) => {
    setMoveTableChoose(e.target.value)
    //get free table id
    let url = Config.API_URL+"/free_table_id"
    fetch(url)
    .then(res=>res.json())
    .then((freeIdArr) => {
      //get choose table
      let chooseId = tableNameIdArr.filter((item)=>item.name===e.target.value)
      chooseId = chooseId[0].id
      //find if available
      let res = freeIdArr.indexOf(chooseId)
      if (res == -1) {
        setNotFreeTableOpen(true)
        setDisableMoveTableButton(true)
        return
      } else {
        setDisableMoveTableButton(false)
      }
    })
  }
  const onTableChange = (e) => {
    setTableChoose(e.target.value)
    //get table id
    let tableId = tableNameIdArr.filter((item)=>item.name===e.target.value)
    tableId = tableId[0].id
    getFoodOrderList(tableId)
  }
  const getFoodOrderList = (tableId) => {
    let url = Config.API_URL+"/get_order_list/"+tableId
    foodOrderListArr = []
    fetch(url)
    .then(res=>res.json())
    .then((data) => {
      data.forEach((item)=> {
        foodOrderListArr.push({
          id:item.id,
          name:item.food.name,
          amount:item.amount,
          detail:item.detail,
          price:item.food.price*item.amount
        })
      })
      setFoodOrderTable([...foodOrderListArr])
      //find total price
      let total = 0
      foodOrderListArr.forEach((item) => {
        total += item.price
      });
      setTotalPrice(total)
      //console.log(foodOrderListArr)
     })
   }
  const onDeleteClick = index => e => {
    setDeleteClickIndex(index)
    setConfirmDeleteOpen(true)
    //console.log(foodOrderListArr)
  }
  const deleteFoodOrder = (e) => {
    let url = Config.API_URL+'/delete_order'
    let sendData = {
      order_id:foodOrderListArr[deleteClickIndex].id
    }
    //console.log(sendData)

    fetch(url,{
      method:'POST',
      body:JSON.stringify(sendData),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(()=> {
      //get table id
      let tableId = tableNameIdArr.filter((item)=>item.name===tableChoose)
      tableId = tableId[0].id
      getFoodOrderList(tableId)
    })
  }


  React.useEffect(()=> {
    //get food category
    let url = Config.API_URL+'/get_table'
    fetch(url)
    .then(res=>res.json())
    .then((data) => {
      data.forEach((item)=> {
        tableArr.push(item.name)
        tableNameIdArr.push({
          id:item.id,
          name:item.name
        })
      })
      setTableSelect(tableArr)
    })
  },[])
  React.useEffect(()=> {
    //get food name
    let url = Config.API_URL+'/food_list'
    fetch(url)
    .then(res=>res.json())
    .then((data) => {
      data.forEach((item)=> {
        foodNameIdPriceArr.push({
          id:item.id,
          name:item.name,
          price:item.price
        })
      })
    })
  },[])

  return (
    <div>
      โต๊ะ
      <Select style={{marginLeft:5}} onChange={onTableChange} value={tableChoose}>
        {
          tableSelect.map((item,i)=> {
            return (
              <MenuItem key={i} value={item}>{item}</MenuItem>
            )
          })

        }
      </Select><hr/>
      <b>รายการที่สั่ง</b>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell><b>ชื่อ</b></TableCell>
              <TableCell><b>จำนวน</b></TableCell>
              <TableCell><b>ราคา</b></TableCell>
              <TableCell><b>เพิ่มเติม</b></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              foodOrderTable.map((item,i) => {
                return (
                  <TableRow key={i}>
                    <TableCell>{item.name}</TableCell>
                    <TableCell>{item.amount}</TableCell>
                    <TableCell>{item.price}</TableCell>
                    <TableCell>{item.detail}</TableCell>
                    <TableCell>
                      <DeleteIcon style={{color:'red'}} onClick={onDeleteClick(i)} />
                    </TableCell>
                  </TableRow>
                )
              })

            }
            <TableRow>
              <TableCell><b>รวมราคา</b></TableCell>
              <TableCell>
                <b>
                  {totalPrice}
                </b>
              </TableCell>

            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
      <br/>
      <ConfirmDialog
        title="แน่ใจหรือ?"
        open={confirmDeleteOpen}
        setOpen={setConfirmDeleteOpen}
        onConfirm={deleteFoodOrder}
      >
        ต้องการลบใช่หรือไม่?
      </ConfirmDialog>
      <Select style={{marginRight:5}} onChange={onMoveTableChange} value={moveTableChoose}>
        {
          tableSelect.map((item,i)=> {
            return (
              <MenuItem key={i} value={item}>{item}</MenuItem>
            )
          })

        }
      </Select>
      <Button variant="contained" disabled={disableMoveTableButton} color="primary" style={{marginRight:100}} onClick={onMoveTableButtonClick}>
        ย้ายโต๊ะ
      </Button>
      <ConfirmDialog
        title="แน่ใจหรือ?"
        open={confirmMoveTableOpen}
        setOpen={setConfirmMoveTableOpen}
        onConfirm={moveTable}
      >
        ต้องการย้ายโต๊ะใช่หรือไม่?
      </ConfirmDialog>
      <Button variant="contained" color="primary" onClick={onCashClick}>
        เก็บเงิน
      </Button>
      <ConfirmDialog
        title="แน่ใจหรือ?"
        open={confirmCashOpen}
        setOpen={setConfirmCashOpen}
        onConfirm={cashTable}
      >
        ต้องการเก็บเงินใช่หรือไม่?
      </ConfirmDialog>
      <Dialog aria-labelledby="customized-dialog-title" open={notFreeTableOpen} onClose={onNotFreeTableClose}>
        <DialogTitle id="customized-dialog-title">
          โต๊ะนี้ไม่ว่าง
        </DialogTitle>
      </Dialog>
    </div>
  )
}

export default CashierComponent;
