import React from 'react'
import {Button,Grid,Select,MenuItem,Table,TableBody,TableCell,TableContainer,TableHead,TableRow,TextField,Icon,Dialog,DialogTitle,DialogActions} from '@material-ui/core'
import ConfirmDialog from './ConfirmDialog'
import HeaderComponent from './HeaderComponent'
import DeleteIcon from '@material-ui/icons/Delete'
import AddIcon from '@material-ui/icons/Add'
import Config from '../config'

var foodNameIdArr = []
var foodNameArr = []
var foodNameCatArr = []
var foodCategoryArr = []
var foodNamePriceArr = []
var tableArr = []
var tableNameIdArr = []

function OrderComponent() {
  const [confirmOpen,setConfirmOpen] = React.useState(false)
  const [foodSelect,setFoodSelect] = React.useState([])
  const [foodCategorySelect,setFoodCategorySelect] = React.useState([])
  const [foodChoose,setFoodChoose] = React.useState('')
  const [foodCategoryChoose,setFoodCategoryChoose] = React.useState('')
  const [foodAmount,setFoodAmount] = React.useState(1)
  const [foodOrderList,setFoodOrderList] = React.useState([])
  const [foodDetail,setFoodDetail] = React.useState('')
  const [tableSelect,setTableSelect] = React.useState([])
  const [tableChoose,setTableChoose] = React.useState('')

  const onFoodSelectChange = (event) => {
    setFoodChoose(event.target.value)
    //console.log(event.target.value)
  }
  const onFoodCategorySelectChange = (event) => {
    setFoodCategoryChoose(event.target.value)
    //filter food with category only
    let resArr = foodNameCatArr.filter(item=> item.cat === event.target.value)
    let resArr2 = []
    resArr.forEach((item)=> {
      resArr2.push(item.name)
    })
    setFoodSelect(resArr2)
  }
  const onFoodAmountChange = (e) => {
    //console.log(e.target.value)
    setFoodAmount(e.target.value)
  }
  const onFoodDetailChange = (e) => {
    setFoodDetail(e.target.value)
  }

  const onAddIconClick = (e) => {
    //e.preventDefault()
    if (foodChoose === '' || foodAmount === '')
      return

    let foodPrice = foodNamePriceArr.filter(item=>item.name===foodChoose)
    foodPrice = foodPrice[0].price

    let data = {
      foodName:foodChoose,
      foodAmount:foodAmount,
      foodPrice:foodPrice,
      foodDetail:foodDetail
    }
    let dataArr = foodOrderList
    dataArr.push(data)
    setFoodOrderList([...dataArr])
    //console.log(foodOrderList)

    setFoodChoose('')
    setFoodCategoryChoose('')
    setFoodAmount(1)
    setFoodDetail('')
  }

  const onDeleteClick = index => e => {
    let foodlist = foodOrderList
    foodlist.splice(index,1)
    setFoodOrderList([...foodlist])
  }

  const onSendClick = async (e) => {
    if (tableChoose == '')
      return
    if (foodOrderList.length == 0 )
      return

    setConfirmOpen(true)
    return
  }

  const sendKitchenData = async () => {
    let data = []
    foodOrderList.forEach(item=> {
      let foodId = foodNameIdArr.filter(item2=> item2.name === item.foodName)
      let tableId = tableNameIdArr.filter(item3=> item3.name === tableChoose)
      data.push({
        foodId:foodId[0].id,
        amount:item.foodAmount,
        detail:item.foodDetail,
        tableId:tableId[0].id
      })
    })
    let sendData = {dataArr:data}
    let url = Config.API_URL+'/send_order'
    //console.log(url)
    fetch(url,{
      method:'POST',
      body:JSON.stringify(sendData),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    //clear orders
    setFoodOrderList([])
  }

  const onTableChange = (e) => {
    //console.log(e.target.value)
    setTableChoose(e.target.value)
  }

  React.useEffect(()=> {
    //get food name
    let url = Config.API_URL+'/food_list'
    fetch(url)
    .then(res=>res.json())
    .then((data) => {
      data.forEach((item)=> {
        foodNameIdArr.push({
          id:item.id,
          name:item.name
        })
        foodNameArr.push(item.name)
        foodNameCatArr.push({
          name:item.name,
          cat:item.category_name,
        })
        foodNamePriceArr.push({
          name:item.name,
          price:item.price
        })
      })
      setFoodSelect(foodNameArr)

    })
  },[])
  React.useEffect(()=> {
    //get food category
    let url = Config.API_URL+'/category_list'
    fetch(url)
    .then(res=>res.json())
    .then((data) => {
      data.forEach((item)=> {
        foodCategoryArr.push(item.name)
      })
      setFoodCategorySelect(foodCategoryArr)
    })
  },[])
  React.useEffect(()=> {
    //get food category
    let url = Config.API_URL+'/get_table'
    fetch(url)
    .then(res=>res.json())
    .then((data) => {
      data.forEach((item)=> {
        tableArr.push(item.name)
        tableNameIdArr.push({
          id:item.id,
          name:item.name
        })
      })
      setTableSelect(tableArr)
    })
  },[])


  return (
    <div>
      โต๊ะ
    <Select style={{marginLeft:5}} onChange={onTableChange}>
      {
        tableSelect.map((item,i)=> {
          return (
            <MenuItem key={i} value={item}>{item}</MenuItem>
          )
        })

      }
    </Select><hr/>
    <b>เพิ่มอาหาร</b><br/>
    หมวด
    <Select style={{marginRight:10,marginLeft:5}} value={foodCategoryChoose} onChange={onFoodCategorySelectChange}>
      {
        foodCategorySelect.map((item,i) => {
          return (<MenuItem key={i} value={item}>{item}</MenuItem>)
        })
      }
    </Select>
    ชื่อ
    <Select style={{marginRight:10,marginLeft:5}} value={foodChoose} onChange={onFoodSelectChange}>
      {
        foodSelect.map((item,i) => {
          return (<MenuItem key={i} value={item}>{item}</MenuItem>)
        })
      }
    </Select>
    จำนวน
    <Select style={{marginRight:10,marginLeft:5}} value={foodAmount} onChange={onFoodAmountChange}>
      <MenuItem key={1} value={1}>1</MenuItem>
      <MenuItem key={2} value={2}>2</MenuItem>
      <MenuItem key={3} value={3}>3</MenuItem>
      <MenuItem key={4} value={4}>4</MenuItem>
      <MenuItem key={5} value={5}>5</MenuItem>
      <MenuItem key={6} value={6}>6</MenuItem>
      <MenuItem key={7} value={7}>7</MenuItem>
      <MenuItem key={8} value={8}>8</MenuItem>
      <MenuItem key={9} value={9}>9</MenuItem>
      <MenuItem key={10} value={10}>10</MenuItem>
    </Select>
    <TextField placeholder="เพิ่มเติม" value={foodDetail} onChange={onFoodDetailChange}/>
    <AddIcon style={{color:"green"}} onClick={onAddIconClick}/>
    <hr/>
    <b>รายการที่สั่ง</b>
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell><b>ชื่อ</b></TableCell>
            <TableCell><b>จำนวน</b></TableCell>
            <TableCell><b>ราคา</b></TableCell>
            <TableCell><b>เพิ่มเติม</b></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            foodOrderList.map((item,i)=> (
                <TableRow key={i}>
                  <TableCell>{item.foodName}</TableCell>
                  <TableCell>{item.foodAmount}</TableCell>
                  <TableCell>{item.foodPrice}</TableCell>
                  <TableCell>{item.foodDetail}</TableCell>
                  <TableCell>
                    <DeleteIcon style={{color:'red'}} onClick={onDeleteClick(i)} />
                  </TableCell>
                </TableRow>
            ))
          }

        </TableBody>
      </Table>
    </TableContainer>
    <br/>
    <Button variant="contained" color="primary" onClick={onSendClick}>ส่งครัว</Button>
    <ConfirmDialog
      title="แน่ใจหรือ?"
      open={confirmOpen}
      setOpen={setConfirmOpen}
      onConfirm={sendKitchenData}
    >
      ต้องการส่งไปครัวใช่หรือไม่?
    </ConfirmDialog>

    </div>
  )
}

export default OrderComponent;
